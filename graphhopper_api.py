# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Utilitaries to query itineraries from GraphHopper."""

from typing import Iterable

import asyncio
from dataclasses import dataclass
import typing as tp
import queue

import aiohttp
import pandas as pd
import numpy as np

from tqdm.asyncio import tqdm


HOST = "localhost"


@dataclass
class TaskPool:
    semaphore: tp.Optional[asyncio.Semaphore]
    tasks: tp.Set[asyncio.Task]
    results: tp.List[tp.Any]
    closed: bool

    def __init__(self, workers: int):
        self.semaphore = asyncio.Semaphore(workers) if workers else None
        self.tasks = set()
        self.results = list()
        self.closed = False
        self.result_queue = queue.Queue()
        self.workers = workers

    async def put(self, coro_f: tp.Callable[[], tp.Awaitable]):
        if self.closed:
            raise RuntimeError("Trying put items into a closed TaskPool")

        if self.semaphore is not None:
            await self.semaphore.acquire()

        task_index = len(self.tasks)
        task = asyncio.ensure_future(self._get_task(coro_f, task_index))
        task.add_done_callback(self._on_task_done)

        self.tasks.add(task)

    async def _get_task(self, coro: tp.Callable[[], tp.Awaitable], task_idx):
        result = await coro
        self.result_queue.put((task_idx, result))

    def _on_task_done(self, _task):
        if self.semaphore is not None:
            self.semaphore.release()

    async def _join(self):
        try:
            await asyncio.gather(*self.tasks)
        finally:
            self.closed = True

            dict_results = {}
            while not self.result_queue.empty():
                task_idx, result = self.result_queue.get()
                dict_results[task_idx] = result

            for i in range(len(dict_results)):
                self.results.append(dict_results[i])

    async def __aenter__(self):
        return self

    async def __aexit__(self, _exc_type, _exc, _tb):
        await self._join()

    def __len__(self) -> int:
        return len(self.tasks)


def _build_urls(
    orix,
    oriy,
    desx,
    desy,
    times,
    port: int,
    mode: str,
):
    orig = (
        np.array(oriy, dtype=str).astype(object)
        + ","
        + np.array(orix, dtype=str).astype(object)
    )
    dest = (
        np.array(desy, dtype=str).astype(object)
        + ","
        + np.array(desx, dtype=str).astype(object)
    )

    urls = (
        f"http://{HOST}:{port}"
        + "/route?instructions=false"
        + "&alternative_route.max_paths=1"
        + "&calc_points=false"
        + "&profile="
        + mode
        + "&point="
        + orig
        + "&point="
        + dest
    )

    if times is not None:
        ftime = (
            times.dt.tz_convert("UTC")
            .apply(pd.Timestamp.isoformat)
            .str.replace("+00:00", "Z")
            .values
        )
        urls += "&pt.arrive_by=true&pt.earliest_departure_time=" + ftime

    return urls


async def _fetch(pbar: tqdm, url: str, session: aiohttp.ClientSession):
    assert isinstance(url, str), f"URL is not a string: {url}"
    awaiting = True
    while awaiting:
        try:
            async with session.get(url) as response:
                response = await response.json()
                if "code" in response:
                    print(f"Error code \"{response['code']}\" encountered.")
                    print(response)
                    print("Retrying...")
                    await asyncio.sleep(1)
                else:
                    awaiting = False
        except (
            aiohttp.ServerDisconnectedError,
            aiohttp.ClientResponseError,
            aiohttp.ClientConnectorError,
        ) as exception:
            print(f"The server connection was dropped on {url}: {exception}")
            await asyncio.sleep(1)
    pbar.update()
    return response


async def _query_urls(urls, workers):
    connector = aiohttp.TCPConnector(limit=None, force_close=True)
    with tqdm(total=len(urls)) as pbar:
        async with aiohttp.ClientSession(connector=connector) as session:
            async with TaskPool(workers) as tasks:
                for url in urls:
                    await tasks.put(_fetch(pbar, url, session))
    assert len(tasks.results) == len(urls)
    return tasks.results


async def get_routes_wrapper(
    origs_x: Iterable,
    origs_y: Iterable,
    dests_x: Iterable,
    dests_y: Iterable,
    times: "Iterable|None",
    mode: str,
    workers: int,
):
    """Get routes from GraphHopper."""
    urls = _build_urls(
        origs_x,
        origs_y,
        dests_x,
        dests_y,
        times,
        8989,
        mode,
    )

    return await _query_urls(urls, workers)  # To run in Jupyter
